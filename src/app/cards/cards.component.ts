import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent {
  @Input() rank = '';
  @Input() suit = '';
  constructor() {
  }
   getClassname(){
      return `card rank-${this.rank.toLowerCase()} ${this.suit}`;
  }
  addIcon(){
     if(this.suit ==='diams'){
       return '♦';
     }else if(this.suit === 'hearts'){
       return '♥';
     }else if(this.suit === 'clubs'){
       return '♣';
     }else if(this.suit === 'spades'){
       return '♠';
     }else{
       return '!!!'
     }
  }

 }
