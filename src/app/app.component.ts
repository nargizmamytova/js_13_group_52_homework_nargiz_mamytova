import {Component, Input} from '@angular/core';
import {Card, CardDeck} from "../lib/CardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'labwork52';

  @Input() ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
  @Input() suits = ['diams', 'hearts', 'clubs', 'spades'];
  cardDeck : CardDeck;
  balanceCardDeck: Card[] = [];
  constructor() {
    this.cardDeck = new CardDeck();
    this.balanceCardDeck = this.cardDeck.getCards(2);
  }
  run(){
    this.cardDeck = new CardDeck();
    this.balanceCardDeck = this.cardDeck.getCards(2)
  }
  giveAnotherCard(){
    const card = this.cardDeck.getCard();
    this.balanceCardDeck.push(card)
  }
  getTotalScore(){
    let sum = 0;
    this.balanceCardDeck.forEach(card => {
      sum +=card.getScore();
    })
    return sum;
  }
}
