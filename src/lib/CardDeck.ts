import {Input} from "@angular/core";
const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const suits = ['diams', 'hearts', 'clubs', 'spades'];
let card: string[] = [];
export class Card {
  constructor(public rank: string,public suit:string) {}
  getScore(): number{
    if(this.rank === "J".toLowerCase() || "Q".toLowerCase() || "K".toLowerCase()){
      return 10;
    }else if(this.rank === 'A'.toLowerCase()){
      return 11;
    }else {
     return  parseInt(this.rank)
    }
  }
}
export class CardDeck{
  cards: Card[] = [];
   constructor() {
     for (let i = 0; i < suits.length; i++) {
        for (let j = 0; j < ranks.length; j++) {
          const suit = suits[i];
          const rank = ranks[j]
          const card = new Card(rank, suit);
          this.cards.push(card);
        }
      }
    }
  getCard(): Card{
    const i = Math.floor(Math.random() * this.cards.length);
    const cardDelete = this.cards.splice(i, 1);
    return cardDelete[0]
  }
  getCards(howMany: number): Card[]{
     const cardDeleteArray: Card[] = [];
    for(let j = 0; j < howMany; j++){
     let cardDeleted = this.getCard();
     cardDeleteArray.push(cardDeleted)
    }
    return cardDeleteArray;
  }

}


